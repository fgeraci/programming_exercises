package utils;

import java.util.List;

import algorithms.structures.Node;

public class Utils {
	public static void printList(List<?> values) {
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for(Object o : values) {
			sb.append(o.toString());				
			if(i < values.size() - 1) {
				sb.append(", ");
			}
			++i;
		}
		System.out.println(sb.toString());
	}
	
	/** Print wrapper
	 * @param s
	 */
	public static void print(String s) { 
		System.out.println(s);
	}
	
	/** Computes the log2 of a;
	 * @param a
	 * @return
	 */
	public static double log2(double a) {
		return (Math.log(a) / Math.log(2.0));
	}
	
	/**
	 * Prints a path of Nodes
	 */
	public static <T extends Comparable<T>> String nodePathString(Node<T> head) {
		Node<T> curr = head;
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		while(curr != null) {
			sb.append(curr.getData().toString() + (curr.getRight() == null ? "]" : ", "));
			curr = curr.getRight();
		}
		return sb.toString();
	}
}
