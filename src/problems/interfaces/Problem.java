package problems.interfaces;

import java.util.Iterator;

import algorithms.structures.LinkedList;

/**
 * Represents a Problem object.
 *
 */
public abstract class Problem {
	
	protected Object[] arguments;
	
	/**
	 * Runs the Problem.
	 */
	public abstract void run();
	
	public Problem(Object[] args) {
		arguments = args;
	}
	
	public Problem() { arguments = null; }
	
	/** Prints the given String **/
	public void println(String str) {
		System.out.println(str);
	}
	
	/** Prints an int[][] matrix **/
	public void printMatrix(int[][] matrix) {
		for(int r = 0 ; r < matrix.length ; r ++) {
			for(int c = 0 ; c < matrix[r].length ; c++) {
				System.out.print(" " + matrix[r][c] + " ");
			}
			println("");
		}
	}
	
	/** Prints a LinkedList<T> object **/
	public <T extends Comparable<T>> void printLinkedList(LinkedList<T> list) {
		StringBuilder s = new StringBuilder("[");
		T data = null;
		Iterator<T> it = list.iterator();
		while(it.hasNext()) {
			data = it.next();
			s.append(data.toString() + (it.hasNext() ? ", " : "")); 
		}
		s.append("]");
		println(s.toString());
	}
}
