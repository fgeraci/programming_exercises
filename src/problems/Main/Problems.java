package problems.Main;

import java.util.ArrayList;
import java.util.List;

import algorithms.structures.LinkedList;
import algorithms.structures.LinkedList.SimplyLinkedList;
import problems.interfaces.Problem;
import problems.problems.CheckIfPermutationOfPalindrome;
import problems.problems.CheckRotatedString;
import problems.problems.CheckStringPermutation;
import problems.problems.CheckUniqueCharactersString;
import problems.problems.FindIntersects;
import problems.problems.FindKthFromLastLinkedList;
import problems.problems.FindPairsInArray;
import problems.problems.FindPermutationPalindrome;
import problems.problems.ModCalculator;
import problems.problems.OneAwaySimilarStrings;
import problems.problems.PartitionLinkedList;
import problems.problems.PositiveIntegersFinder;
import problems.problems.RemoveDuplicatesLinkedList;
import problems.problems.RemoveMiddleElementLinkedList;
import problems.problems.RotateMatrix;
import problems.problems.RotateMatrixInPlace;
import problems.problems.StringCompression;
import problems.problems.StringPermutations;
import problems.problems.URLifyString;
import problems.problems.ZeroOutMatrixRowAndCol;

/**
 * This class will run different instances of the Problem class.
 *
 */
public class Problems {

	public static void main(String[] args) {
		List<Problem> problems = new ArrayList<>();
		
		// Arrays, Lists and Strings
		problems.add(new PositiveIntegersFinder(new Object[] { 5 }));
		problems.add(new StringPermutations(new Object[] { "abc" }));
		problems.add(new ModCalculator(new Object[] { 100, 30 }));
		problems.add(new FindPairsInArray(new Object[] { new int[]{ 1,7,5,9,2,12,3 } , 2} ));
		problems.add(new FindIntersects(new Object[] {new Integer[]{13,27,35,40,49,55,59},new Integer[]{17,35,39,40,55,58,60}}));
		problems.add(new CheckUniqueCharactersString(new Object[] {new String[] {"abccdefg", "abcdefg", "abcdeff"}}));
		problems.add(new CheckStringPermutation(new Object[] {"abc", "abb"}));
		problems.add(new URLifyString(new Object[] {"https://my.company.org?hello=my     baby     "}));
		problems.add(new FindPermutationPalindrome(new Object[] {"Tact Coa"}));
		problems.add(new CheckIfPermutationOfPalindrome(new Object[] {"racecar"}));
		problems.add(new OneAwaySimilarStrings(new Object[] {"pale", "bake"}));
		problems.add(new StringCompression(new Object[] {"aaabbbcc"}));
		problems.add(new RotateMatrix(new Object[] { new int[][]{ {1,0,0,0}, {1,0,0,0}, {1,0,0,0}, {1,1,1,1} } }));
		problems.add(new RotateMatrixInPlace(new Object[] { new int[][]{ {1,0,0,0}, {1,0,0,0}, {1,0,0,0}, {1,1,1,1} } }));
		problems.add(new ZeroOutMatrixRowAndCol(new Object[] { new int[][]{ {1,1,1,1}, {1,1,1,1}, {1,1,0,1}, {1,1,1,1}, {1,1,1,1} } }));
		problems.add(new CheckRotatedString(new Object[] {"waterbottle", "terbottlewa"} ));
		
		// Linked Lists
		LinkedList<Integer> simplyLinkedList = new SimplyLinkedList<>();
		simplyLinkedList.add(8); simplyLinkedList.add(-1); simplyLinkedList.add(1); simplyLinkedList.add(33);
		simplyLinkedList.add(1); simplyLinkedList.add(5); simplyLinkedList.add(22); simplyLinkedList.add(8); 
		simplyLinkedList.add(1);
		LinkedList<Integer> simplyLinkedListB = new SimplyLinkedList<>();
		simplyLinkedListB.add(9); simplyLinkedListB.add(22); simplyLinkedListB.add(-1); simplyLinkedListB.add(31);
		simplyLinkedListB.add(3); simplyLinkedListB.add(44); simplyLinkedListB.add(7);  simplyLinkedListB.add(28); 
		simplyLinkedListB.add(-1); 
		problems.add(new RemoveDuplicatesLinkedList(new Object[] { simplyLinkedList }));
		problems.add(new FindKthFromLastLinkedList(new Object[] {simplyLinkedListB, 3}));
		problems.add(new RemoveMiddleElementLinkedList(new Object[] {simplyLinkedListB, 3}));
		LinkedList<Integer> simplyLinkedListC = new SimplyLinkedList<>();
		simplyLinkedListC.add(3); simplyLinkedListC.add(5); simplyLinkedListC.add(8); simplyLinkedListC.add(5);
		simplyLinkedListC.add(10); simplyLinkedListC.add(2); simplyLinkedListC.add(1);
		problems.add(new PartitionLinkedList(new Object[] {simplyLinkedListC , 5}));
		
		for(Problem p : problems) {
			String currentClass = " --- " + p.getClass().getSimpleName() + " --- ";
			print(currentClass);
			p.run();
			print(currentClass);
		}
	}
	
	// UTILS
	
	/*
	 * Prints-line a string.
	 */
	public static void print(String s) {
		System.out.println(s);
	}
}
