package problems.problems;

import algorithms.structures.LinkedList;
import algorithms.structures.LinkedList.SimplyLinkedList;
import algorithms.structures.Node;
import problems.interfaces.Problem;


/**
 * Given only access to a middle node, remove it from the LinkedList.
 * We do this by copying the value of next in this node, and 
 * removing the next node by shifting the reference.
 * 
 * If we get the reference as a parameter, this is a O(1) operation.
 *
 */
public class RemoveMiddleElementLinkedList extends Problem {
	
	public RemoveMiddleElementLinkedList(Object[] args) {
		super(args);
	}
	
	@SuppressWarnings("unchecked")
	public void run() {
		LinkedList<?> ll = (SimplyLinkedList<?>) arguments[0]; // just for checking.
		Integer nodeVal = (Integer) arguments[1]; // the Node to be removed.
		println("List before removing node " + nodeVal + " ...");
		printLinkedList(ll);
		// Necessary for set up
		Node<Integer> curr = (Node<Integer>) ll.getListHead();
		while(curr.getData() != nodeVal) {
			curr = curr.getRight();
		}
		// Crux
		if(curr.getRight() != null) {
			curr.setData(curr.getRight().getData());
			curr.setRight(curr.getRight().getRight());
		}
		println("List after removing node " + nodeVal + " ...");
		printLinkedList(ll);
	}
}
