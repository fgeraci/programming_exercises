package problems.problems;

import java.util.HashSet;
import java.util.Set;

import problems.interfaces.Problem;

/**
 * Check if a given string is made up of unique characters.
 *
 */
public class CheckUniqueCharactersString extends Problem {
	
	public CheckUniqueCharactersString(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		String[] strs = (String[]) arguments[0];
		// Way A
		// Way 3 - Using a DS (HashSet) O(n)
		for(String s : strs) {
			Set<Character> chars = new HashSet<>();
			boolean unique = true;
			for(int i = 0 ; i < s.length(); i++) {
				char curr = s.charAt(i);
				if(chars.contains(curr)) {
					unique = false; break;
				}
				else chars.add(s.charAt(i));
			}
			if(unique)
				System.out.println(s + " - UNIQUE");
			else System.out.println(s + " - NOT UNIQUE");
		}
		// Way B
		// O(n) - Not using a DS, but a 32-bit int to flip bits as we find characters. Only works for a-z
		for(String s : strs) {
			boolean unique = true;
			int bitArray = 0;
			s = s.toLowerCase();
			for(int i = 0 ; i < s.length(); i++) {
				int asciiVal = (int) ( s.charAt(i) - 'a');
				if((bitArray & (1 << asciiVal)) > 0) {
					unique = false; break;
				}
				bitArray = (bitArray | 1 << asciiVal);
			}
			if(unique)
				System.out.println(s + " - Bits - UNIQUE");
			else System.out.println(s + " - Bits - NOT UNIQUE");
		}
		// Way C - Sorting based on ASCII code-values, O(n*log n)
	}
}
