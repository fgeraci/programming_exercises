package problems.problems;

import problems.interfaces.Problem;

public class StringCompression extends Problem {
	
	public StringCompression(Object[] args) { super(args); }
	
	@Override
	public void run() {
		String original = (String) arguments[0];
		StringBuilder sb = new StringBuilder();
		if(original.length() == 0) println("Compressed string: " + original);
		else if (original.length() == 1) println("Compressed string: " + original + 1);
		else {
			char curr = original.charAt(0), prev = original.charAt(0);
			int count = 0;
			for(int i = 1 ; i < original.length() ; i++) {
				curr = original.charAt(i);
				if(curr == prev) {
					count++;
				} else {
					sb.append(prev + "" + (count + 1));
					count = 0;
				}
				prev = curr;
			}
			sb.append(prev + "" + (count + 1));
			println("Compressed string: " + original + " -> " + sb.toString());
		}
	}
}
