package problems.problems;

import java.util.HashMap;
import java.util.Map;

import algorithms.structures.LinkedList;
import algorithms.structures.LinkedList.SimplyLinkedList;
import algorithms.structures.Node;
import problems.interfaces.Problem;

/**
 * Return the Kth to Last element from a LinkedList
 *
 */

public class FindKthFromLastLinkedList extends Problem {

	public FindKthFromLastLinkedList(Object[] args) {
		super(args);
	}
	
	public void run() {
		LinkedList<?> ll = (SimplyLinkedList<?>) arguments[0];
		printLinkedList(ll);
		int k = (int) arguments[1];
		println("Finding the Kth " + k + " from last element in...");
		Map<Integer,Node<?>> support = new HashMap<>();
		int idx = 0;
		Node<?> curr = ll.getListHead();
		while(curr != null) {
			support.put(idx,curr);
			curr = curr.getRight();
			idx++;
		}
		if(idx < k) { 
			println("K is greater than the list's length");
		} else {
			println("Found: " + support.get(idx-k).getData().toString());
		}
	}
	
}
