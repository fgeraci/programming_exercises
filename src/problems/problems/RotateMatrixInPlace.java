package problems.problems;

import problems.interfaces.Problem;

public class RotateMatrixInPlace extends Problem {

	public RotateMatrixInPlace(Object[] args) {
		super(args);
	}
	
	public void run() {
		int[][] matrix = (int[][]) arguments[0];
		int it = 0, bound = matrix.length - 1;
		println("Before rotate: ");
		printMatrix(matrix);
		for(int r = 0 ; r < matrix.length / 2; r++) {
			for(int j = 0 + it ; j < matrix.length - it - 1; j++) {
				int valA = matrix[r][j];
				int valB = matrix[j][bound - it];
				int valC = matrix[bound - it][bound - j - it];
				int valD = matrix[bound - it - j][r];
				matrix[r][j] = valD;
				matrix[j][bound - it] = valA;
				matrix[bound - it][bound - j - it] = valB;
				matrix[bound - it - j][r] = valC;
			}
			it++;
		}
		println("Rotated matrix: ");
		printMatrix(matrix);
	}
}
