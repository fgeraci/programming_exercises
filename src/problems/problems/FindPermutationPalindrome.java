package problems.problems;

import java.util.ArrayList;
import java.util.List;

import problems.interfaces.Problem;

/**
 * Given a string, check if it is the permutation of a palindrome and finds the palindrome.
 * 
 * For example: "Tact cao" << permuted: "taco cat" which is a palindrome.
 * 
 * First approach: find all permutations, check if they are palindromes.
 * Terrible complexity, O(!n)
 */
public class FindPermutationPalindrome extends Problem {
	
	public FindPermutationPalindrome(Object[] args) {
		super(args);
	}
	
	public void run() {
		String str = (String) arguments[0];
		str = str.toLowerCase();
		List<String> palindromes = new ArrayList<>();
		permutation(str, "", palindromes);
		if(palindromes.size() > 0) {
			println(str + " is a permutation of the palindrome: " + palindromes.get(0));
		} else {
			println(str + " is a not permutation of a palindrome");
		}
	}
	
	private void permutation(String prefix, String permutation, List<String> palindromes) {
		if(palindromes.size() > 0 || prefix.length() == 0) {
			permutation = permutation.replace(" ", "");
			for(int i = 0, j = permutation.length() - 1; i <= j; i++, j--) {
				if(permutation.charAt(i) != permutation.charAt(j)) break;
				else if (i == j) palindromes.add(permutation); // palindrome found
			}
		} else {
			for(int i = 0; i < prefix.length(); i++) {
				String remainder = prefix.substring(0,i) + prefix.substring(i + 1);
				permutation(remainder, permutation + prefix.charAt(i), palindromes);
				if (palindromes.size() > 0) break;
			}
		}
	}
}
