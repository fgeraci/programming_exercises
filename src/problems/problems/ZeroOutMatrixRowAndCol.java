package problems.problems;

import problems.interfaces.Problem;

/**
 * If a value is 0, in an N*M matrix, 0 out the entire column and row.
 *
 */
public class ZeroOutMatrixRowAndCol extends Problem {
	
	public ZeroOutMatrixRowAndCol(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		int[][] matrix = (int[][]) arguments[0];
		println("Zeroing out cols/rows in matrix:");
		printMatrix(matrix);
		zeroer_loop: 
			for(int row = 0; row < matrix.length ; row++) {
				for(int col = 0; col < matrix[row].length; col++) {
					if(matrix[col][row] == 0) {
						int offset = 1;
						while(	row - offset >= 0 || 
								row + offset < matrix.length || 
								col - offset >= 0 ||
								col + offset < matrix[row].length) {
							if(row - offset >= 0) matrix[row - offset][col] = 0; // ^
							if(row + offset < matrix.length) matrix[row+offset][col] = 0; // v
							if(col - offset >= 0) matrix[row][col-offset] = 0; // <
							if(col + offset < matrix[row].length) matrix[row][col+offset] = 0;
							offset++;
						}
						break zeroer_loop;
					}
				}
			}
		println("After:");
		printMatrix(matrix);
	}
}
