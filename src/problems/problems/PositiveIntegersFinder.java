package problems.problems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import problems.interfaces.Problem;

/**
 * Finds all positive solutions, with integers between 1 and 1000, of:
 * a^3 + b^3 = c^3 + d^3
 *
 */
public class PositiveIntegersFinder extends Problem {

	private int upperLimit;
	
	public PositiveIntegersFinder(Object[] args) {
		super(args);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public void run() {
		// pre-compute pairs c^3+d^3 results - O(N^2)
		upperLimit = (Integer) this.arguments[0];
		Map<Integer, List<Tuple>> results = new HashMap<>();
		for(int c = 1 ; c <= upperLimit ; c++ ) {
			for(int d = 1 ; d <= upperLimit ; d++ ) {
				int result = (int) (Math.pow(c, 3) + Math.pow(d, 3));
				if(!results.containsKey(results))
					results.put(result, new ArrayList<Tuple>());
				results.get(result).add(new Tuple(c,d));
			}
		}
		for(int a = 1 ; a <= upperLimit ; a++) {
			for(int b = 1 ; b <= upperLimit ; b++) {
				int result = (int) (Math.pow(a, 3) + Math.pow(b,3));
				for(Tuple t : results.get(result)) {
					System.out.println(a 
							+ "^3 + " + b + "^3 = " + t.getA() + "^3 + " + t.getB() + "^3");
				}
			}
		}
	}
	
	private class Tuple {
		int A;
		int B;
		public Tuple(int a, int b) {
			A = a; B = b;
		}
		public int getA() { return A; }
		public int getB() { return B; }
	}
}
