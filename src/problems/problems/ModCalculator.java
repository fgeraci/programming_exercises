package problems.problems;

import problems.interfaces.Problem;

public class ModCalculator extends Problem {
	
	public ModCalculator(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		int a =	(int) this.arguments[0];
		int b =	(int) this.arguments[1];
		int div = a / b;
		int mod = a - div * b;
		System.out.println(a + " mod " + b + " = " + mod);
	}
}
