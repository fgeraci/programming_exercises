package problems.problems;

import problems.interfaces.Problem;

/**
 * Replaces spaces with %20%
 * GOTCHA: when appending chars, they need to be casted to Strings with a + "" operation.
 * @author fgera
 *
 */
public class URLifyString extends Problem {
	
	public URLifyString(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		String str = (String) arguments[0];
		str = str.toLowerCase();
		StringBuilder sb = new StringBuilder();
		str = str.trim();
		for(int i = 0; i < str.length(); i++) {
			char curr = str.charAt(i);
			if(curr == ' ') {
				if(i != 0 && str.charAt(i-1) == ' ') continue;
				sb.append("%20%");
			}
			else sb.append(curr + ""); // cast it to a String
		}
		println("Original String: " + str);
		println("URLified String: " + sb.toString());
	}

}