package problems.problems;

import java.util.HashMap;
import java.util.Map;

import problems.interfaces.Problem;

/**
 * Find all pairs in an array with a positive difference of k.
 *
 */

public class FindPairsInArray extends Problem {
	
	public FindPairsInArray(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		int[] a = (int[]) this.arguments[0];
		int k = (int) this.arguments[1];
		Map<Integer,Integer> values = new HashMap<>();
		for(int i : a) {
			values.put(i, Integer.MIN_VALUE);
		}
		for(int i : a) {
			int dist = i - k;
			if(values.containsKey(dist)) {
				System.out.println("(" + dist + "," + i + ")" );
			}
		}
	}
	
}
