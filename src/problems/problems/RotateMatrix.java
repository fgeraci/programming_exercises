package problems.problems;

import problems.interfaces.Problem;

/**
 * Rotates a given N*N matrix by 90 degress in place.
 *
 */

public class RotateMatrix extends Problem {

	public RotateMatrix(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		int[][] matrix = (int[][]) arguments[0];
		Integer[][] support = new Integer[matrix.length][matrix.length];
		int colB = matrix.length - 1;
		println("Rotating matrix: ");
		printMatrix(matrix);
		for(int r = 0; r < matrix.length ; r ++) {
			for(int c = 0 ; c < matrix.length ; c++) {
				int currVal = support[r][c] != null ? support[r][c] : matrix[r][c], 
						currValB = matrix[c][colB - r];
				matrix[c][colB - r] = currVal;
				support[c][colB - r] = currValB;
			}
		}
		println("--> 90 degrees ... ");
		printMatrix(matrix);
	}
	
}
