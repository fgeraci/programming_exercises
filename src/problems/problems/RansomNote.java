package problems.problems;

import java.util.HashMap;
import java.util.Map;

import problems.interfaces.Problem;

public class RansomNote extends Problem {
	
	public RansomNote(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		String ransomNote = (String) arguments[0];
		String magazine = (String) arguments[1];
		Map<Character,Integer> characters = new HashMap<>();
		for(int i = 0 ; i < magazine.length(); i++) {
			char currChar = magazine.charAt(i);
			if(Character.isAlphabetic(currChar)) {
				if(!characters.containsKey(currChar)) {
					characters.put(currChar,0);
				}
				characters.put(currChar,characters.get(currChar) + 1);
			}
		}
		for(int c = 0 ; c < ransomNote.length() ; c++) {
			char currChar = ransomNote.charAt(c);
			if(Character.isAlphabetic(currChar)) {
				if(characters.containsKey(currChar) && characters.get(currChar) > 0) {
					
				}
			}
		}
	}
}
