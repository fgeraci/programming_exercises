package problems.problems;

import problems.interfaces.Problem;

public class StringPermutations extends Problem {
	
	public StringPermutations(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		String permute = (String) this.arguments[0];
		permute(permute,"");
	}
	
	void permute(String str, String pref) {
		if(str.length() == 0)
			System.out.println(pref);
		else {
			for(int i = 0; i < str.length(); i++) {
				String rem = str.substring(0,i) + str.substring(i + 1);
				permute(rem, pref + str.charAt(i));
			}
		}
	}
	
}
