package problems.problems;

import algorithms.structures.LinkedList;
import algorithms.structures.Node;
import problems.interfaces.Problem;

/**
 * Given a LinkedList and a value X, a list must be partitioned
 * such that values less than X, appear before in the list, while
 * values greater or equal, appear after. Pretty much a Quicksort
 * partitioning exercises with LLs.
 *
 * Two chains of nodes will be created. The left one, will maintain
 * a tail pointer, while the right one a head pointer.
 * After the list has been completely iterated, both lists will be
 * linked utilizing the tail of left with the head of right.
 *
 *
 * OBSERVATION: ideally, the allocation code should be put in a method,
 * the problem is that Java passes by value the address, within the JVM of
 * the reference of an object, hence passing the initial NULL of the left
 * and right partitions, would not allow for the non exiting object to be modified.
 */

public class PartitionLinkedList extends Problem {
	
	public PartitionLinkedList(Object[] args) {
		super(args);
	}
	
	@SuppressWarnings("unchecked")
	public void run() {
		LinkedList<Integer> ll = (LinkedList<Integer>) arguments[0];
		println("Partitioning: ");
		printLinkedList(ll);
		int valueX = (int) arguments[1];
		Node<Integer> leftTail = null, leftHead = null, right = null, curr = ll.getListHead(), prev = null;
		if(curr.getRight() != null) {
			prev = curr; curr = curr.getRight();
			while(curr != null) {
				prev.setRight(null);
				if(prev.getData().compareTo(valueX) < 0) {
					if(leftTail == null) {
						leftHead = leftTail = prev;
					}
					else { 
						leftTail.setRight(prev);
						leftTail = leftTail.getRight();
					}
				} else {
					if(right != null) prev.setRight(right);
					right = prev;
				}
				prev = curr; curr = curr.getRight();
			}
			prev.setRight(null);
			if(prev.getData().compareTo(valueX) < 0) { leftTail.setRight(prev); leftTail = prev; }
			else { 
				prev.setRight(right);
				right = prev;
			}
			leftTail.setRight(right);
			ll.setListHead(leftHead);
			printLinkedList(ll);
		}
	}

}
