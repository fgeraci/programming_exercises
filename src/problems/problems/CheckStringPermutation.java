package problems.problems;

import java.util.HashMap;
import java.util.Map;

import problems.interfaces.Problem;

/**
 * Given two strings, check if one is a permutation of the other one.
 * Complexity: O(n)
 *
 */
public class CheckStringPermutation extends Problem {
	
	public CheckStringPermutation(Object[] args) {
		super(args);
	}
	
	public void run() {
		boolean permutation = true;
		String a = (String) arguments[0], b = (String) arguments[1];
		if(a.isEmpty() || (a.length() != b.length())) permutation = false;
		else {
			Map<Character,Integer> charsFreq = new HashMap<>();
			for(int c = 0; c < a.length(); c++) {
				char curr = a.charAt(c);
				if(!charsFreq.containsKey(curr)) charsFreq.put(curr, 0);
				charsFreq.put(curr, charsFreq.get(curr) + 1);
			}
			for(int c = 0; c < b.length(); c++) {
				char curr = b.charAt(c);
				if(charsFreq.containsKey(curr)) {
					if(charsFreq.get(curr) > 0) {
						charsFreq.put(curr,charsFreq.get(curr) - 1);
					} else { permutation = false; break; }
				} else { permutation = false; break; }
			}
			for(Integer i : charsFreq.values()) {
				if(i != 0) permutation = false; break;
			}
			if(permutation)
				println("\"" + a + "\"" + " and " + "\"" + b + "\"" + " are permutations");
			else
				println("\"" + a + "\"" + " and " + "\"" + b + "\"" + " are NOT permutations");
		}
		
	}

}
