package problems.problems;

import algorithms.sort.Mergesort;
import algorithms.structures.LinkedList.SimplyLinkedList;
import algorithms.structures.Node;
import problems.interfaces.Problem;

/**
 * Remove duplicates from a LinkedList
 * Problem 1 - We can use a support DS.
 * Problem 2 - Without using a support DS.
 *
 */

public class RemoveDuplicatesLinkedList extends Problem {

	public RemoveDuplicatesLinkedList(Object[] args) {
		super(args);
	}
	
	@SuppressWarnings("unchecked")
	public void run() {
		SimplyLinkedList<Integer> linkedList = (SimplyLinkedList<Integer>) arguments[0];
		printLinkedList(linkedList);
		Mergesort.sort(linkedList);
		println("Merge sorring linked list ...");
		printLinkedList(linkedList);
		Node<Integer> curr = linkedList.getListHead();
		while(curr.getRight() != null) {
			if(curr.getData().compareTo(curr.getRight().getData()) == 0) {
				curr.setRight(curr.getRight().getRight());
			} else {
				curr = curr.getRight();
			}
		}
		println("Removed duplicates ...");
		printLinkedList(linkedList);
	}
	
}
