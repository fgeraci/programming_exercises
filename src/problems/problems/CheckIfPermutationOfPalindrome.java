package problems.problems;

import java.util.HashMap;
import java.util.Map;

import problems.interfaces.Problem;

public class CheckIfPermutationOfPalindrome extends Problem {
	
	public CheckIfPermutationOfPalindrome(Object[] args) {
		super(args);
	}
	
	public void run() {
		boolean test = true;
		String str = (String) arguments[0];
		str = str.replace(" ", ""); str = str.toLowerCase();
		Map<Character,Integer> pairs = new HashMap<>();
		int tgtPairs = str.length() / 2, tgtSingles = str.length() % 2 == 0 ? 0 : 1;
		for(int i = 0 ; i < str.length(); i++) {
			char curr = str.charAt(i);
			if(!pairs.containsKey(curr)) pairs.put(curr,0);
			pairs.put(curr,pairs.get(curr) + 1);
		}
		int singlesCount = 0, pairsCount = 0;
		for(Integer i : pairs.values()) {
			if(i > 2) { test = false; break; }
			singlesCount = i == 1 ? singlesCount + 1 : singlesCount;
			pairsCount = i == 2 ? pairsCount + 1 : pairsCount;
		}
		if(test && singlesCount == tgtSingles && pairsCount == tgtPairs)
			println(str + " is a permutation of a palindrome");
		else {
			println(str + " is NOT a permutation of a palindrome");
		}
	}

}
