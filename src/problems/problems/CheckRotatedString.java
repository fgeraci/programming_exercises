package problems.problems;

import problems.interfaces.Problem;

/**
 * Given two strings, s1 and s2, find if s2 is a rotation of s1.
 * 
 * waterbottle ---> terbottlewa
 *
 */
public class CheckRotatedString extends Problem {
	
	
	public CheckRotatedString(Object[] args) {
		super(args);
	}
	
	public void run() {
		String s1 = (String) arguments[0];
		String s2 = (String) arguments[1];
		boolean rotation = false;
		mainTest : if(s1 != null && !s1.isEmpty() && s1.length() == s2.length()) {
			int s1Length = s1.length();
			char s1Char = s1.charAt(0);
			for(int i = 0 ; i < s2.length() ; i++) {
				int offset = 0, matched = 0;
				char match = s2.charAt(i);
				while(match == s1Char) {
					matched++;
					if(matched == s1Length) { rotation = true; break mainTest; } 
					else {
						offset++;
						s1Char = s1.charAt(offset);
						match = s2.charAt((i + offset) % s1Length);
					}
				}
			}
		}
		if(rotation) println(s2 + " is a rotation of " + s1);
		else println(s2 + " is not a rotation of " + s1);
	}
	
}
