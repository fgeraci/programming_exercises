package problems.problems;

import java.util.ArrayList;
import java.util.List;

import problems.Main.Problems;
import problems.interfaces.Problem;

/**
 * Given to sorted arrays, find all the intersecting elements.
 * 1. Choose the shortest array - A
 * 2. for each element of A, do a linear search on B until
 *  we find a greater value, move to the next element of A,
 *	repeat the search starting from the last index of B.
 *
 *	Complexity: O(max(|A|,|B|))
 */
public class FindIntersects extends Problem {
	
	public FindIntersects(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		List<Integer> intersects = new ArrayList<>();
		Integer[] arrA = (Integer[]) arguments[0], arrB = (Integer[]) arguments[1];
		Integer[] A = arrA.length < arrB.length ? arrA : arrB;
		Integer[] B = A == arrA ? arrB : arrA;
		int j = 0;
		for(int i = 0 ; i < A.length ; i++) {
			int currVal = A[i];
			while(j < B.length && B[j] <= currVal) {
				if(B[j] == currVal) {
					Problems.print("Intersect: " + currVal);
					intersects.add(currVal);
				}
				j++;
			}
		};
	}
	
}
