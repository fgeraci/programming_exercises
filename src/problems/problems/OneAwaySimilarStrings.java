package problems.problems;

import java.util.HashMap;
import java.util.Map;
import problems.interfaces.Problem;

/**
 * A string can have 1 character added, edited or removed.
 * Given two strings, check if they are 1 or less changes from each other.
 * Current solution complexity:
 *		string 1 = A, string 2 = B
 *		A + B = O(max(|A|,|B|) or O(N) if |A| = |B|
 */

public class OneAwaySimilarStrings extends Problem {
	
	public OneAwaySimilarStrings(Object[] args) {
		super(args);
	}
	
	@Override
	public void run() {
		String a = (String) arguments[0], b = (String) arguments[1];
		if(a.equalsIgnoreCase(b)) printSuccess();
		else if(Math.abs(a.length() - b.length()) > 1) printFailure();
		else {
			if(Math.abs(a.length() - a.length()) <= 1 ) {
				Map<Character,Integer> charsA = new HashMap<>();
				for(int i = 0; i < a.length(); i++) {
					char curr = a.charAt(i);
					if(!charsA.containsKey(curr)) charsA.put(curr, 0);
					charsA.put(curr,charsA.get(curr) + 1);
				}
				for(int j = 0; j < b.length(); ++j) {
					char curr = b.charAt(j);
					if(charsA.containsKey(curr)) {
						int vals = charsA.get(curr) - 1;
						if(vals == 0) charsA.remove(curr);
						else charsA.put(curr, vals);
					}	
				}
				if(charsA.size() == 0 && (b.length() - a.length() <= 1)) { // b is one edit away 	
					printSuccess();
				} else if((charsA.size() == 1) && ((a.length() - b.length()) <= 1)) { // one edit, both same length
					printSuccess();
				} else {
					printFailure();
				}
			} else {
				printFailure();
			}
		}
	}
	
	private void printSuccess() {
		String a = (String) arguments[0], b = (String) arguments[1];
		println("At most one away! " + a + " and " + b + " are the same.");
	}
	
	private void printFailure() {
		String a = (String) arguments[0], b = (String) arguments[1];
		println("More than one away :( " + a + " and " + b + " are NOT the same.");
	}
}
