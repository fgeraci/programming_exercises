package algorithms.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import utils.Utils;

/**
 * Quicksort Implementation
 *
 */
public class Quicksort {
	
	/**
	 * Applies the Quicksort algorithm to a list of values.
	 * @param Comparable<E> objects
	 * @param values
	 */
	public static <E extends Comparable<E>> List<E> sort(List<E> values) {
		if(values.size() <= 1) return values;
		List<E> sortedList = new ArrayList<>();
		// Select an arbitrary pivot.
		int pivotIdx = (new Random()).nextInt(values.size());
		E pivot = values.get(pivotIdx);
		// Populate left and right
		List<E> left = new ArrayList<>(), right = new ArrayList<>();
		// Split
		for(int i = 0 ; i < values.size(); ++i) {
			if(i == pivotIdx) continue;
			int res = pivot.compareTo(values.get(i));
			if(res >= 0) {
				left.add(values.get(i));
			} else {
				right.add(values.get(i));
			}
		}
		// Sort
		left = sort(left);
		right = sort(right);
		// Merge in order
		sortedList.addAll(left);
		sortedList.add(pivot);
		sortedList.addAll(right);
		return sortedList;
	}
}
