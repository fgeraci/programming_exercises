package algorithms.sort;

import algorithms.structures.LinkedList;
import algorithms.structures.Node;
import utils.Utils;

/** 
 * Implement the Mergesort algorithm.
 *
 */

public class Mergesort {
	
	public static <T extends Comparable<T>> void sort(LinkedList<T> list) {
		list.setListHead(mergeSort(list.getListHead()));
	}
	
	private static <T extends Comparable<T>> Node<T> mergeSort(Node<T> head) {
		// Only keep going if it is the last element
		Node<T> curr = head;
		if(curr.getRight() == null)  {
			return curr;
		}
		else {
			// Divide
			Node<T> prev = curr;
			curr = curr.getRight();
			// Move the pointers prev + 1 and curr + 2 each time. Split the list in two pieces.
			while(curr != null && curr.getRight() != null) {
				prev = prev.getRight();
				curr = curr.getRight() != null ? curr.getRight().getRight() : curr.getRight();
			}
			Node<T> right = prev.getRight();
			prev.setRight(null);
			Node<T> left = mergeSort(head);
			right = mergeSort(right);
			// Merge
			Node<T> tail = null; head = null;
			while(left != null && right != null) {
				Node<T> next = null;
				if(left.getData().compareTo(right.getData()) < 0) {
					next = left; left = left.getRight();
				} else {
					next = right; right = right.getRight();
				}
				if(tail == null) head = tail = next;
				else { tail.setRight(next); tail = next; }
			}
			if(left != null) tail.setRight(left);
			else if (right != null) { tail.setRight(right); }
			// Deal with the remainder
			return head;
		}
	}	
}
