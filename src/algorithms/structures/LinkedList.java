package algorithms.structures;

import java.util.Iterator;

/**
 * Classic linked list implementation
 *
 */
public abstract class LinkedList<T extends Comparable<T>> implements Iterable<T> {
	
	protected int size;
	
	public LinkedList() {
		this.listHead = this.listTail = null;
	}
	
	protected Node<T> listHead;
	protected Node<T> listTail;
	
	/**
	 * Adds a single element to the list, by default, it accepts duplicates.
	 * @param data
	 * @return true if succeeded
	 */
	public abstract boolean add(T data);
	
	/**
	 * Removes the first occurrence of a matching element from the list.
	 * @param data
	 */
	public abstract boolean remove(T data);
	public abstract boolean removeAll(T data);
	public abstract boolean search(T data);
	
	public void mergeSort() {
		
	}
	
	public int size() {
		return this.size;
	}
	
	@Override
	public String toString() {
		String listStr = "size: "+ size +" - [";
		if(this.size == 0) return listStr + "]";
		Node<T> curr = this.listHead;
		while(curr != null) {
			listStr += curr.getData().toString() + (curr.getRight() == null ? "]" : ", ");
			curr = curr.getRight();
		}
		return listStr;
	}
	
	public Iterator<T> iterator() {
		return new LinkedListIterator<T>(this);
	}
	
	public void setListHead(Node<T> head) {
		this.listHead = head;
	}
	
	public Node<T> getListHead() {
		return this.listHead;
	}
	
	public Node<T> getListTail() {
		return this.listTail;
	}
	
	/** Custom Iterator **/
	private static class LinkedListIterator<T extends Comparable<T>> implements Iterator<T> {
		
		private Node<T> current;
		
		public LinkedListIterator(LinkedList<T> list) {
			current = list.listHead;
		}
		
		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public T next() {
			T data = current.getData();
			current = current.getRight();
			return data;
		}
		
		
	}
	
	public static class DoublyLinkedList<T extends Comparable<T>> extends LinkedList<T> {

		@Override
		public boolean add(T data) {
			Node<T> newNode = new Node<>(data);
			if(this.listHead == null) {
				listHead = newNode;
				return true;
			} else {
				this.listTail.setRight(newNode);
				newNode.setLeft(this.listTail);
				this.listTail = newNode;
			}
			return true;
		}

		@Override
		public boolean remove(T data) {
			Node<T> prev = null, curr = this.listHead;
			while(curr != null) {
				if(curr.getData() == data) {
					if(prev != null) prev.setRight(curr.getRight()); // data is in the head
					if(curr.getRight() != null) curr.getRight().setLeft(prev); // data is in the tail
					return true;
				} else {
					prev = curr;
					curr = curr.getRight();
				}
			}
			return false;
		}

		@Override
		public boolean removeAll(T data) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean search(T data) {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	public static class SimplyLinkedList<T extends Comparable<T>> extends LinkedList<T> {
		
		public boolean add(T data) {
			try {
				if(this.listHead == null) {
					this.listTail = this.listHead = new Node<>(data);
				} else {
					Node<T> newNode = new Node<>(data);
					this.listTail.setRight(newNode);
					this.listTail = newNode;
				}
				this.size++;
				return true;
			} catch (Exception e) { return false; } 
		}
		
		public boolean remove(T data) {
			Node<T> curr = this.listHead, prev = null;
			while(curr != null) {
				if(curr.getData().compareTo(data) == 0) {
					if (curr == this.listHead) {
						this.listHead = curr.getRight();
					} else {
						prev.setRight(curr.getRight());
					}
					this.size--;
					return true;
				}
				prev = curr; curr = curr.getRight();
			}
			return false; 
		}
		
		public boolean removeAll(T data) { return false; }
		public boolean search(T data) { return false; }
		
	}

}
