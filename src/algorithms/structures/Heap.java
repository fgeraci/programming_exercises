package algorithms.structures;

import java.util.ArrayList;
import java.util.List;

import utils.Utils;

/**
 * A heap is a complete binary tree.
 * A max heap indicates that each child is smaller than its parent.
 * A heap is represented as a tree-like structure, but internally can
 * be arranged as an array or list.
 * It supports the following functions: 
 * 	Extract max/min (depends on type of heap)
 * 	Insert
 * 	Remove
 * 	Search
 * 
 * Properties: 
 *	Children:
 *		Left:  2+i + 1 
 *		Right: 2+i + 2
 *	Parent: (i-1) / 2
 *
 * To heapify:
 * 	For each element on the array
 * 		insert at bottom
 * 		while not at the first element, check parent, then swap if bigger/smaller.
 * 
 *
 */
public class Heap<T extends Comparable<T>> {
	
	List<T> heapArray;
	
	public Heap() {
		heapArray = new ArrayList<>();
	}
	
	public Heap(T[] array) {
		heapify(array);
	}
	
	/**
	 * Adds a new value into the heap
	 * @param el
	 */
	public void insert(T el) {
		heapArray.add(el);
		// Swap to top
		int currentIndex = heapArray.size() - 1;
		while(currentIndex > 0) {
			int parentIndex =  (currentIndex - 1) / 2;
			if(heapArray.get(currentIndex).compareTo(heapArray.get(parentIndex)) < 0 ) {
				swap(currentIndex,parentIndex);
				currentIndex = parentIndex;
			} else break;
		}
	}
	
	/**
	 * Extract the root, move the tail to the head, sift the value down log n times.
	 * @return
	 */
	public T extract() {
		if(heapArray.size() > 0) {
			T data = heapArray.remove(0);			
			if(heapArray.size() > 1) {
				T tail = heapArray.remove(heapArray.size() -1 );
				heapArray.add(0,tail);
				// sift down
				int currentIndex = 0;
				while(currentIndex < heapArray.size() - 1) {
					int leftChild = (currentIndex * 2) + 1, rightChild = (currentIndex * 2) + 2;
					T curr = heapArray.get(currentIndex);
					T left = heapArray.get(leftChild);
					T right = rightChild <= heapArray.size() - 1 ? heapArray.get(rightChild) : null;
					if(left.compareTo(curr) < 0) {
						if(right != null && right.compareTo(left) < 0) {
							swap(currentIndex,rightChild);
							currentIndex = rightChild;
						} else {
							swap(currentIndex, leftChild);
							currentIndex = leftChild;
						}
					} else if(right != null && right.compareTo(curr) < 0) {
						swap(currentIndex,rightChild);
						currentIndex = rightChild;
					} else { break; }
				}
			}
			return data;
		} else return null;
	}
	
	public void printHeapArray() {
		Utils.printList(heapArray);
	}
	
	/*
	 * Swaps two values in the internal array
	 */
	private void swap(int from, int to) {
		T fromNode = heapArray.get(from), toNode = heapArray.get(to);
		heapArray.set(to, fromNode);
		heapArray.set(from,toNode);
	}
	
	/*
	 * Heapifying costs O(N * log N)
	 */
	private void heapify(T[] array) {
		heapArray = new ArrayList<>(array.length);
		for(T el : array) {
			// Insert
			insert(el);
		}
	}
}
