package algorithms.structures;
/**
 * Generic Node for data structures such as
 * Linked Lists, Heaps and Binary Trees.
 *
 * Left and Right members can be also interpreted
 * as previous (left) and next (right)
 *
 * @param <T>
 */
public class Node<T> {
	
	private Node<T> left;
	private Node<T> right;
	
	private T data;
	
	/* Constructors */
	public Node() { }
	
	public Node(T data) {
		this.data = data;
	}
	
	public Node(Node<T> left, Node<T> right) {
		this.left = left;
		this.right = right;
	}
	
	public Node(Node<T> left, Node<T> right, T data) {
		this(left,right);
		this.data = data;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	public T getData() {
		return this.data;
	}
	
	public void setLeft(Node<T> left) {
		this.left = left;
	}
	
	public void setRight(Node<T> right) {
		this.right = right;
	}
	
	public Node<T> getLeft() {
		return this.left;
	}
	
	public Node<T> getRight() {
		return this.right;
	}
}
