package algorithms.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import algorithms.sort.Quicksort;
import algorithms.structures.Heap;
import algorithms.structures.LinkedList;
import utils.Utils;

/**
 * Tester and driver
 *
 */
public class Main {
	// Tester
		public static void main(String[] args) {
			
			// Quicksort
			List<Integer> intValues = new ArrayList<>(Arrays.asList(5,-11,33,101,67,9,3,1));
			List<String> strValues = new ArrayList<>(Arrays.asList("f","d","e","a","b","c","g","h","z","s","q","t","u"));
			try {
				// Numbers
				Utils.printList(Quicksort.sort(intValues));
				// Strings
				Utils.printList(Quicksort.sort(strValues));
			} catch (Exception e) {
				System.out.println("Exception Quicksorting: " + e.getMessage());
			}
			Utils.print("---- HEAP ----");
			// Heap
			Heap<Integer> heap = new Heap<>(new Integer[] {90,7,55,4,50,87});
			heap.printHeapArray();
			heap.insert(1);
			heap.printHeapArray();
			Utils.print("Extract: " + heap.extract());
			heap.printHeapArray();
			Utils.print("Extract: " + heap.extract());
			heap.printHeapArray();
			Utils.print("Extract: " + heap.extract());
			heap.printHeapArray();
			heap.insert(4);
			Utils.print("Insert 4");
			heap.printHeapArray();
			Utils.print("---- LINKED LIST ----");
			// Linked List
			LinkedList<Integer> linkedList = new LinkedList.SimplyLinkedList<>();
			linkedList.add(5);
			linkedList.add(10);
			linkedList.add(9);
			Utils.print(linkedList.toString());
			linkedList.remove(10);
			Utils.print(linkedList.toString());
			linkedList.remove(5);
			Utils.print(linkedList.toString());
			linkedList.remove(9);
			Utils.print(linkedList.toString());
			linkedList.add(5);
			linkedList.add(10);
			Utils.print(linkedList.toString());
		}
}
