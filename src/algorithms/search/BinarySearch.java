package algorithms.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import algorithms.sort.Quicksort;
import utils.Utils;

public class BinarySearch {

	public static void main(String[] args) {
		List<Integer> intValues = new ArrayList<>(Arrays.asList(5,-11,33,101,67,9,3,1));
		intValues = Quicksort.sort(intValues);
		Integer[] arrayValues =  new Integer[intValues.size()];
		for(int i = 0 ; i < intValues.size() ; ++i) arrayValues[i] = intValues.get(i); 
		int tgtVal = -11;
		Utils.printList(intValues);
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, intValues) ? "found." : "not found."));
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, arrayValues) ? "found." : "not found."));
		tgtVal = 101;
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, intValues) ? "found." : "not found."));
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, arrayValues) ? "found." : "not found."));
		tgtVal = 9;
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, intValues) ? "found." : "not found."));
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, arrayValues) ? "found." : "not found."));
		tgtVal = 10;
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, intValues) ? "found." : "not found."));
		System.out.println("The value " + tgtVal + " was " + (exists(tgtVal, arrayValues) ? "found." : "not found."));
	}
	
	public static <E extends Comparable<E>> boolean exists(E element, E[] values) {
		return search(0, values.length - 1, element,values);
	}
	
	private static <E extends Comparable<E>> boolean search(int min, int max, E element, E[] values) {
		if(min == max) return element.compareTo(values[min]) == 0;
		else {
			int middle = (min + max) / 2;
			int res = element.compareTo(values[middle]);
			if(res == 0) return true;
			else if (res < 0) {
				return search(min, middle - 1, element, values);
			} else {
				return search(middle + 1, max, element, values);
			}
		}
	}
	
	/**
	 * Searches for an element, within a sorted list.
	 * Compleity: O(lg N)
	 * No need to compute the middle es (MIN+MAX) / 2 since the list is halved
	 * each time.
	 * If the algorithm would be using an array, then we would need to keep track
	 * of the min and max for each iteration.
	 * @param <E>
	 * @param element
	 * @param values
	 * @return
	 */
	public static <E extends Comparable<E>> boolean exists(E element, List<E> values) {
		if(values.size() == 1) return element.compareTo(values.get(0)) == 0;
		else {
			int middle = values.size() / 2;
			int res = element.compareTo(values.get(middle));
			if(res == 0)
				return true;
			else if (res < 0) {
				return exists(element,values.subList(0, middle));
			} else {
				return exists(element,values.subList(middle, values.size()));
			}
		}
	}
	
}
